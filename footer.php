	<footer class="narrow">
		<div class="wrapper">

			<div class="info">
				<h3><?php echo get_field('footer_headline', 'options'); ?></h3>
				<?php echo get_field('footer_copy', 'options'); ?>

				<div class="cta">
					<a href="mailto:<?php echo get_field('email', 'options'); ?>">Email us</a>
				</div>
			</div>

		</div>
	</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>