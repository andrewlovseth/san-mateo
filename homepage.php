<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<h1><?php echo get_field('page_header_headline'); ?></h1>
			<h2><?php echo get_field('page_header_sub_headline'); ?></h2>
			
		</div>
	</section>


	<section id="quotes" class="slideshow">
		<?php if(have_rows('quotes')): while(have_rows('quotes')): the_row(); ?>
		 
		    <article class="quote">
		    	<div class="photo">
		    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    	</div>

		    	<div class="copy">
		    		<div class="copy-wrapper">

			    		<blockquote>
			    			<?php echo get_sub_field('quote'); ?>
			    		</blockquote>

			    		<cite>
			    			<h4>&mdash; <?php echo get_sub_field('source'); ?></h4>
			    			<h5><?php echo get_sub_field('source_description'); ?></h5>
			    		</cite>		

		    		</div>
		    	</div>
		    </article>

		<?php endwhile; endif; ?>

	</section>


	<section id="call-to-arms">
		<div class="wrapper">
			
			<h2><?php echo get_field('call_to_arms_headline'); ?></h2>

		</div>
	</section>


	<section id="about" class="narrow">
		<div class="wrapper">
			
			<h2 class="section-header"><?php echo get_field('about_headline'); ?></h2>

			<?php if(have_rows('about_content')): while(have_rows('about_content')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'paragraph' ): ?>
					
					<div class="paragraph">
			    		<?php echo get_sub_field('copy'); ?>
					</div>
					
			    <?php endif; ?>

			    <?php if( get_row_layout() == 'inset' ): ?>
					
					<div class="inset">
						<div class="sub-headline">
							<h4><?php echo get_sub_field('sub_headline'); ?></h4>
						</div>
						<div class="copy">
							<h3><?php echo get_sub_field('headline'); ?></h3>
							<?php echo get_sub_field('copy'); ?>
						</div>
			    		
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>


	<section id="mission" class="narrow">
		<div class="wrapper">
		
			<h2 class="section-header"><?php echo get_field('mission_headline'); ?></h2>

			<?php if(have_rows('mission_features')): while(have_rows('mission_features')): the_row(); ?>
			 
			    <div class="feature">
			    	<div class="icon">
			    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

			    	<div class="copy paragraph">
			    		<?php echo get_sub_field('copy'); ?>			    		
			    	</div>
			    </div>

			<?php endwhile; endif; ?>


		</div>
	</section>

	
	<section id="priorities" class="narrow">
		<div class="wrapper">

			<h3 class="section-header"><?php echo get_field('priorities_headline'); ?></h3>

			<div class="paragraph">
				<?php echo get_field('priorities_copy'); ?>
			</div>			
			
			<div class="documents-wrapper">
				
				<?php if(have_rows('priorities_docs')): while(have_rows('priorities_docs')): the_row(); ?>
				 
				    <div class="doc">
				    	<div class="image">
				    		<a href="<?php echo get_sub_field('link'); ?>" rel="external">
				    			<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    		</a>
				    	</div>
				    	<div class="info">
				    		<a href="<?php echo get_sub_field('link'); ?>" rel="external"><?php echo get_sub_field('link_label'); ?></a>
				    	</div>
				        
				    </div>

				<?php endwhile; endif; ?>
				
			</div>

		</div>
	</section>


<?php get_footer(); ?>