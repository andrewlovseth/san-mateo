<?php

/*

	Template Name: Background

*/

get_header(); ?>


	<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
	 
	    <?php if( get_row_layout() == 'section' ): ?>
			
			<section class="info-section narrow">
				<div class="wrapper">

					<h2 class="section-header"><?php echo get_sub_field('headline'); ?></h2>
					<div class="paragraph">
						<?php echo get_sub_field('copy'); ?>
					</div>

				</div>
			</section>
			
	    <?php endif; ?>
	 
	<?php endwhile; endif; ?>


<?php get_footer(); ?>