<?php

/*

	Template Name: Who We Are

*/

get_header(); ?>

	<section id="page-header" class="narrow">
		<div class="wrapper">

			<h2 class="section-header"><?php echo get_field('page_header_headline'); ?></h2>
			
		</div>
	</section>


	<section id="member-list" class="narrow">
		<div class="wrapper">
			
			<h3 class="section-header"><?php echo get_field('member_list_headline'); ?></h3>

			<?php if(have_rows('member_list')): while(have_rows('member_list')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'list_section' ): ?>
					
					<table class="list">
						<thead>
							<th class="name-col"<?php if(!get_sub_field('second_column_label')): ?> colspan="2"<?php endif; ?>><?php echo get_sub_field('name_column_label'); ?></th>
							<?php if(get_sub_field('second_column_label')): ?>
								<th class="second-col"><?php echo get_sub_field('second_column_label'); ?></th>
							<?php endif; ?>
						</thead>

						<tbody>
							<?php if(have_rows('names')): while(have_rows('names')): the_row(); ?>
								<tr>
									<td class="name-col"><?php echo get_sub_field('name'); ?></td>
									<td class="second-col"><?php echo get_sub_field('second_column_value'); ?></td>
								</tr>
							<?php endwhile; endif; ?>
						</tbody>
					</table>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>


	<section id="entities" class="narrow">
		<div class="wrapper">
			
			<h3 class="section-header"><?php echo get_field('entities_headline'); ?></h3>

			<div class="paragraph">
				<?php echo get_field('entities_copy'); ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>