<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<header>
		<div class="wrapper">

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>

			<div class="identity">
				<div class="site-title">
					<h1>
						<a href="<?php echo site_url('/'); ?>">
							<?php echo get_field('site_title', 'options'); ?>
						</a>
					</h1>
				</div>

				<div class="site-tagline">
					<h2><?php echo get_field('site_tagline', 'options'); ?></h2>
				</div>
			</div>

		</div>
	</header>

	<nav>
		<div class="wrapper">

			<div class="links">
				<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
					<a href="<?php echo get_sub_field('link'); ?>" class="nav-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
						<span><?php echo get_sub_field('label'); ?></span>
					</a>
				<?php endwhile; endif; ?>
			</div>

		</div>
	</nav>
