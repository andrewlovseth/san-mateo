<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<section id="page-header" class="narrow">
		<div class="wrapper">

			<h2 class="section-header"><?php echo get_field('page_header_headline'); ?></h2>

			<div class="paragraph">
				<?php echo get_field('page_header_copy'); ?>
			</div>
			
		</div>
	</section>


	<section id="contact-form" class="narrow">
		<div class="wrapper">

		
			<?php 
				$shortcode = get_field('contact_form_shortcode');
				echo do_shortcode($shortcode);
			?>


		</div>
	</section>

<?php get_footer(); ?>