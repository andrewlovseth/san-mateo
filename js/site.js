$(document).ready(function() {

	// rel="external"
	$('a[rel="external"], section.document-list a').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Smooth Scroll
	$('a.smooth').smoothScroll();


	// Menu Toggle
	$('#toggle').click(function(){
		$('header').toggleClass('open');
		$('nav').slideToggle();
		return false;
	});


	// Slideshow
	$('.slideshow').slick({
		arrows: true,
		adaptiveHeight: true
	});

});