<?php

/*

	Template Name: Resources

*/

get_header(); ?>

	<section id="page-header" class="narrow">
		<div class="wrapper">

			<h2 class="section-header"><?php echo get_field('page_header_headline'); ?></h2>

			<div class="paragraph">
				<?php echo get_field('page_header_copy'); ?>
			</div>
			
		</div>
	</section>


	<section id="downloads" class="narrow">
		<div class="wrapper">

			<div class="documents-wrapper">
				<?php if(have_rows('documents')): while(have_rows('documents')): the_row(); ?>
				 
				    <div class="doc">
				    	<div class="image">
				    		<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>
				    	<div class="info">
				    		<a href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('link_label'); ?></a>
				    	</div>
				        
				    </div>

				<?php endwhile; endif; ?>
			</div>				

		</div>
	</section>


	<?php if(have_rows('document_section')): while(have_rows('document_section')) : the_row(); ?>
	 
	    <?php if( get_row_layout() == 'section' ): ?>
			
			<section class="document-list narrow">
				<div class="wrapper">

					<div class="info-wrapper">
						<h3 class="section-header"><?php echo get_sub_field('headline'); ?></h3>

						<div class="paragraph">
							<?php echo get_sub_field('copy'); ?>
						</div>
					</div>

				</div>
			</section>
			
	    <?php endif; ?>
	 
	<?php endwhile; endif; ?>


<?php get_footer(); ?>